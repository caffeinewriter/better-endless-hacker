This is a pretty messy userscript to improve the quality of life on
EndlessHacker. It's not terribly featureful just yet, but it has some important
features, like cooldown notifications, and easier navigation via key combos.

Documentation on key combos coming soon, but for now, just read the blob. 
Key combinations are Gmail style. E.g. "Press h then j".