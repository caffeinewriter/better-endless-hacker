// ==UserScript==
// @name     Better Endless Hacker
// @version  2
// @include  /^https?:\/\/.*\.endlesshacker\.com.*$/
// @exclude  /^https?:\/\/.*\.endlesshacker\.com/guest.*$/
// @require  https://craig.global.ssl.fastly.net/js/mousetrap/mousetrap.min.js?a4098
// @require  https://rawcdn.githack.com/ccampbell/mousetrap/215621acf801a04d2cba4c1f990fb3844b9243d8/plugins/global-bind/mousetrap-global-bind.min.js
// @grant    GM.openInTab
// @grant    GM.notification
// @grant    GM.getValue
// @grant    GM.setValue
// @grant    unsafeWindow
// @resource ehLogo https://www.endlesshacker.com/theme/images/logo6.png
// ==/UserScript==

const BEH_AudioPlayer = document.createElement("audio");
BEH_AudioPlayer.src =
  "https://glcdn.githack.com/caffeinewriter/better-endless-hacker/raw/089cd95b/alertone.mp3";
BEH_AudioPlayer.preload = "auto";

const BEHCommands = {
  go: {
    action: BEH_CommandGo,
    autocomplete: BEH_AutocompleteGo
  }
};

let BEHLocations = {
  hacklogs: {
    shortcut: "c l h",
    action: BEH_GoToLoc("?h=readlog&log=hack")
  },
  logs: {
    shortcut: "c l o",
    action: BEH_GoToLoc("?h=logs")
  },
  systemlogs: {
    shortcut: "c l s",
    action: BEH_GoToLoc("?h=readlog&log=system")
  },
  transferlogs: {
    shortcut: "c l t",
    action: BEH_GoToLoc("?h=readlog&log=transfer")
  },
  serverlogs: {
    shortcut: "c l v",
    action: BEH_GoToLoc("?h=readlog&log=server")
  },
  servermanager: {
    shortcut: "c m",
    action: BEH_GoToLoc("?h=servermanager")
  },
  system: {
    shortcut: "c s",
    action: BEH_GoToLoc("?h=system")
  },
  transfers: {
    shortcut: "c t",
    action: BEH_GoToLoc("?h=transfers")
  },
  submitform: {
    shortcut: "d s",
    action: BEH_ActionConfirm(
      "Would you like to submit this form?",
      () =>
        document.querySelector("form[method=POST].alt-design") &&
        document.querySelector("form[method=POST].alt-design").submit()
    )
  },
  condef: {
    shortcut: "e b",
    action: BEH_GoToLoc("?h=backuponline")
  },
  condef: {
    shortcut: "e c",
    action: BEH_GoToLoc("?h=convention")
  },
  economy: {
    shortcut: "e e",
    action: BEH_GoToLoc("?h=economy")
  },
  shoppingmall: {
    shortcut: "e s",
    action: BEH_GoToLoc("?h=shop&shop=isp")
  },
  specialshop: {
    shortcut: "e p",
    action: BEH_GoToLoc("?h=shop&shop=hackshop")
  },
  blackjack: {
    shortcut: "g b",
    action: BEH_GoToLoc("?h=doblackjack")
  },
  games: {
    shortcut: "g g",
    action: BEH_GoToLoc("?h=games")
  },
  instantlotto: {
    shortcut: "g i",
    action: BEH_GoToLoc("?h=lottery_instant")
  },
  lotto: {
    shortcut: "g l",
    action: BEH_GoToLoc("?h=lottery")
  },
  slots: {
    shortcut: "g s",
    action: BEH_GoToLoc("?h=doslots")
  },
  npccontract: {
    shortcut: "h c",
    action: BEH_GoToLoc("?h=npcmission")
  },
  hackfbi: {
    shortcut: "h f",
    action: BEH_GoToLoc("?h=fbiloginpage")
  },
  hacker4hire: {
    shortcut: "h h",
    action: BEH_GoToLoc("?h=hacker4hire")
  },
  infections: {
    shortcut: "h i",
    action: BEH_GoToLoc("?h=infections")
  },
  smalljobs: {
    shortcut: "h j",
    action: BEH_GoToLoc("?h=joblist")
  },
  missions: {
    shortcut: "h m",
    action: BEH_GoToLoc("?h=missions")
  },
  hackapc: {
    shortcut: "h p",
    action: BEH_GoToLoc("?h=hackapc")
  },
  hackaserver: {
    shortcut: "h s",
    action: BEH_GoToLoc("?h=hackaserver")
  },
  publicchat: {
    shortcut: "i c",
    action: BEH_GoToLoc("?h=chat")
  },
  publicftps: {
    shortcut: "i f",
    action: BEH_GoToLoc("?h=ftplist")
  },
  internetmap: {
    shortcut: "i m",
    action: BEH_GoToLoc("?h=internet")
  },
  bounties: {
    shortcut: "l b",
    action: BEH_GoToLoc("?h=bountyboard")
  },
  fbimostwanted: {
    shortcut: "l f",
    action: BEH_GoToLoc("?h=fbimostwanted")
  },
  jail: {
    shortcut: "l j",
    action: BEH_GoToLoc("?h=jail")
  },
  prison: {
    shortcut: "l p",
    action: BEH_GoToLoc("?h=prison")
  },
  achievements: {
    shortcut: "m a",
    action: BEH_GoToLoc("?h=achievements")
  },
  mybank: {
    shortcut: "m b",
    action: BEH_GoToLoc("?h=dobankaccount&account=hacker")
  },
  ephistory: {
    shortcut: "m e",
    action: BEH_GoToLoc("?h=history&type=ep")
  },
  friendsandfoes: {
    shortcut: "m f",
    action: BEH_GoToLoc("?h=list")
  },
  myprofile: {
    shortcut: "m p",
    action: () => document.querySelector("#sidebar #nickname").click()
  },
  mystats: {
    shortcut: "m s",
    action: BEH_GoToLoc("?h=personalstats")
  },
  worldrank: {
    shortcut: "w r",
    action: BEH_GoToLoc("?h=worldranks")
  },
  worldstats: {
    shortcut: "w s",
    action: BEH_GoToLoc("?h=stats2")
  }
};

function BEH_IsJailed() {
  return !!document.querySelectorAll("#jail #countdown").length;
}

function BEH_GoToLoc(loc) {
  return () => (document.location = loc);
}

function BEH_ActionConfirm(conf, act) {
  return () => {
    if (confirm(conf)) {
      act();
    }
  };
}

function BEH_TimeDelta(a, b) {
  a = a.split(":").map(Number);
  b = b.split(":").map(Number);
  let actions = ["setHours", "setMinutes", "setSeconds"];
  let dateDelta = 0;
  let aDate = new Date();
  let bDate = new Date();
  if (a[0] > b[0]) dateDelta = 1;
  bDate.setDate(bDate.getDate() + dateDelta);
  for (let i = a.length; !!i; i--) {
    aDate[actions[i - 1]](a[i - 1]);
    bDate[actions[i - 1]](b[i - 1]);
  }
  return bDate - aDate;
}

function BEH_RegisterDownloadTimeWarnings() {
  const chooseResetTime = currentTime => {
    const resetTimes = ["4:00:00", "10:00:00", "16:00:00", "22:00:00"];
    let curHour = parseInt(currentTime.split(":")[0]);
    if (curHour >= 4 && curHour < 10) {
      return resetTimes[1];
    } else if (curHour >= 10 && curHour < 16) {
      return resetTimes[2];
    } else if (curHour >= 16 && curHour < 22) {
      return resetTimes[3];
    } else {
      return resetTimes[0];
    }
  };
  let buyButtons = document.querySelectorAll("[name=h][value=docart]");
  buyButtons.forEach(v => {
    let container = v.parentElement.parentElement.parentElement;
    let dlTime =
      container
        .getElementsByClassName("shop-item-details")[0]
        .textContent.replace(/.+\[D\/L: ([^ ]+).+/gi, "$1")
        .replace(/[^\d:]/gi, "")
        .split(":")
        .reverse()
        .map((v, k) => parseInt(v) * Math.pow(60, k))
        .reduce((v, a) => v + a) * 1000;
    if (Number.isNaN(dlTime)) return;
    v.parentElement.addEventListener("submit", function(e) {
      let serverTime = document.getElementById("servertime").textContent;
      let delta = BEH_TimeDelta(serverTime, chooseResetTime(serverTime));
      BEH_log(dlTime, delta, serverTime);
      if (delta - dlTime < 60000)
        !confirm(
          `Are you sure you'd like to continue? The download will take ${Math.floor(
            dlTime / 1000
          )} seconds, and the shop IP resets in ${Math.floor(
            delta / 1000
          )} seconds.`
        ) && e.preventDefault();
    });
  });
}

function BEH_RegisterKeyCombos() {
  for (let loc in BEHLocations) {
    Mousetrap.bind(BEHLocations[loc].shortcut, BEHLocations[loc].action);
  }
}

function BEH_log() {
  console.log("[BEH]", ...arguments);
}

function BEH_PopulateAutocompleteResults(res) {
  let rbox = document.getElementById("command-results");
  rbox.innerHTML = "";
  for (let i of res) {
    let el = document.createElement("li");
    el.innerHTML = i;
    rbox.appendElement(el);
  }
}

function BEH_CreateNotificationTimeout(obj, time) {
  setTimeout(() => {
    console.log("Sending notification for", obj.name);
    BEH_AudioPlayer.play();
    GM.notification(
      obj.text,
      obj.title,
      "https://www.endlesshacker.com/theme/images/logo6.png",
      obj.action
    );
  }, time * 1000);
}

function BEH_CreateReadyAlerts() {
  let timers = {
    temp_scount: {
      name: "Servers",
      title: "Server Hack Ready",
      text: "Your server hack cooldown is complete.",
      action: () => {
        document.location = "?h=hackaserver";
      }
    },
    temp_pcount: {
      name: "PC",
      title: "PC Hack Ready",
      text: "Your PC hack cooldown is complete.",
      action: () => {
        document.location = "?h=hackapc";
      }
    },
    temp_jcount: {
      name: "Small Job",
      title: "Small Job Ready",
      text: "Your small job cooldown is complete.",
      action: () => {
        document.location = "?h=joblist";
      }
    },
    temp_mcount: {
      name: "Contract",
      title: "NPC Contract Ready",
      text: "Your NPC contract cooldown is complete.",
      action: () => {
        document.location = "?h=npcmission";
      }
    },
    temp_dcount: {
      name: "Downloads",
      title: "Download Complete",
      text: "All your downloads have completed.",
      action: () => {
        document.location = "?h=transfers";
      }
    },
    temp_countdown: {
      name: "Jail",
      title: "Free From Jail",
      text: "You have been released from jail.",
      action: () => {
        document.location = document.location;
      } // pseudo refresh to avoid POST data resends
    }
  };
  for (i in timers) {
    if (BEH_IsJailed() ^ (i === "temp_countdown")) {
      // Only create temp_countdown if jailed. Otherwise, create all others except temp_countdown
      continue;
    }
    let timeDelta = unsafeWindow[i] - Date.now() / 1000 + 1;
    if (timeDelta > 0) {
      BEH_log("Setting notification timeout for", i);
      BEH_CreateNotificationTimeout(timers[i], timeDelta);
    }
  }
}

function BEH_Autocomplete(sub) {
  let args = sub.split(" ");
  if (args.length === 1) {
    args[0];
  }
}

function BEH_AutocompleteGo(sub) {}

function BEH_CommandGo(loc) {}

function BEH_ToggleCommandBox() {
  let mod = document.getElementById("command-modal");
  let nextDis =
    mod.style.display === "none" || !mod.style.display ? "block" : "none";
  mod.style.display = nextDis;
  if (nextDis !== "none") {
    let cb = document.getElementById("command-box");
    cb.focus();
  }
}

function BEH_Ready() {
  const modalContent = `
		<style>
			#command-modal {
				position: fixed;
				top: 33%;
				left: 50%;
				z-index: 12000;
				transform: translate(-50%, -50%);
				border: 5px solid #555;
				border-radius: 5px;
				width: 33%;
				display: none;
			}
			#command-box {
				width: 100%;
				height: 50px;
				font-size: 36px;
				font-family: monospace;
				background-color: #111;
				color: #0F0;
			}
			#command-results {
				list-style-type: none;
			}
			#comamnd-results li {
 				background-color: #555;
			}
			#command-results li:nth-child(even) {
				background-color: #777;
			}
		</style>
    <div id="command-modal">
      <input id="command-box"/>
      <ul id="command-results">
      </ul>
    </div>
  `;

  const modalElement = document.createElement("div");
  modalElement.innerHTML = modalContent;
  document.body.appendChild(modalElement);
  Mousetrap.bindGlobal("alt+c", (e, handler) => {
    e.preventDefault();
    BEH_ToggleCommandBox();
  });
  BEH_CreateReadyAlerts();
  BEH_RegisterKeyCombos();
  if (document.querySelector("#shop,#docart")) {
    BEH_log("Registering download time warnings.");
    BEH_RegisterDownloadTimeWarnings();
  }
  BEH_log("BEH Ready");
  BEH_log("Is jailed?", BEH_IsJailed());
}

(function() {
  BEH_log("BEH Starting...");
  BEH_Ready();
})();
